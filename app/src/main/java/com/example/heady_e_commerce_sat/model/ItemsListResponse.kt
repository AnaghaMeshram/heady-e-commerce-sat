package com.example.heady_e_commerce_sat.model

data class ItemsListResponse(
    val categories: ArrayList<Category>,
    val rankings: ArrayList<Ranking>
)

data class Category(
    val id: Int,
    val name: String,
    val products: ArrayList<Product>,
    val child_categories: ArrayList<Int>
)

data class Product(
    val id: Int,
    val name : String,
    val variants : ArrayList<Variants>,
    val tax : Tax)

data class Variants(
    val id: Int,
    val color : String,
    val size : Int,
    val price : Int
)

data class Tax(
    val name : String,
    val value : Float
)

data class Ranking(
    val products: List<RankedProduct>,
    val ranking: String
)

data class RankedProduct(
    val id: Int,
    val view_count : Int,
    val order_count : Int,
    val shares : Int

)