package com.example.shaadi_demo_app.network

import android.content.Context
import android.graphics.Bitmap
import android.util.DisplayMetrics
import android.util.LruCache
import com.android.volley.toolbox.ImageLoader.ImageCache

/**
 * Created by Anagha Meshram on 25/06/2019.
 */


class LruBitmapCache(maxSize: Int) : LruCache<String, Bitmap>(maxSize), ImageCache {

    constructor(ctx: Context) : this(getCacheSize(ctx)) {}

    override fun sizeOf(key: String, value: Bitmap): Int {
        return value.rowBytes * value.height
    }

    override fun getBitmap(url: String): Bitmap {
        return get(url)
    }

    override fun putBitmap(url: String, bitmap: Bitmap) {
        put(url, bitmap)
    }

    companion object {

        // Returns a cache size equal to approximately three screens worth of images.
        fun getCacheSize(ctx: Context): Int {
            val displayMetrics = ctx.resources.displayMetrics
            val screenWidth = displayMetrics.widthPixels
            val screenHeight = displayMetrics.heightPixels
            // 4 bytes per pixel
            val screenBytes = screenWidth * screenHeight * 4

            return screenBytes * 3
        }
    }
}
