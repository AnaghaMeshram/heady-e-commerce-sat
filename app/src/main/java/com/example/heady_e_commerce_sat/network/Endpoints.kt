package com.example.sambhaji.plexusbee.network

/**
 * Created by Anagha Meshram on 25/06/2019.
 */

open class Endpoints {

    private val mHEADY_DOMAIN = "https://stark-spire-93433.herokuapp.com/"


    private val mITEMS_LIST = "json"



    fun getItemsListURL(): String {
        return mHEADY_DOMAIN + mITEMS_LIST
    }


}