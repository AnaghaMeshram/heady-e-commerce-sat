package com.example.heady_e_commerce_sat

import android.os.Bundle
import android.util.Log
import android.util.Log.d
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.SubMenu
import android.view.View
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.heady_e_commerce_sat.adapter.ExpandableListAdapter
import com.example.heady_e_commerce_sat.adapter.ItemsListAdapter
import com.example.heady_e_commerce_sat.model.Category
import com.example.heady_e_commerce_sat.model.ItemsListResponse
import com.example.heady_e_commerce_sat.model.Product
import com.example.sambhaji.plexusbee.network.Endpoints
import com.example.shaadi_demo_app.network.VolleySingleton
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.Array.get
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,ExpandableListAdapter.SubCategoryClickListener {

    val TAG = MainActivity::class.java!!.simpleName
    var categoryItemsListArrayList: ArrayList<Category> = ArrayList<Category>()
    var productItemsListArrayList: ArrayList<Product> = ArrayList<Product>()
    val menu: MutableList<String> = ArrayList()
    val subMenu: MutableList<MutableList<String>> = ArrayList()
    var drawerLayout : DrawerLayout? = null
    var gson = Gson()
    lateinit var mItemsListAdapter: ItemsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        init()
        showAllItemAPI()
        addDrawerItems()

    }

    private fun addDrawerItems() {

        navList.setOnGroupExpandListener(ExpandableListView.OnGroupExpandListener { groupPosition ->
            Toast.makeText(
                this,
                (menu.get(groupPosition)).toString() + " List Expanded.",
                Toast.LENGTH_SHORT
            ).show()
        })

        navList.setOnGroupCollapseListener(ExpandableListView.OnGroupCollapseListener { groupPosition ->
            Toast.makeText(
                this,
                (menu.get(groupPosition)).toString() + " List Collapsed.",
                Toast.LENGTH_SHORT
            ).show()
        })


    }

    override fun onSubmenuClick(selectedValue: String?) {

        Toast.makeText(this,selectedValue,Toast.LENGTH_SHORT).show()
        showFilteredItems(selectedValue)
        drawerLayout!!.closeDrawer(GravityCompat.START)
    }

    private fun init() {

        drawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout


        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout!!.addDrawerListener(toggle)
        toggle.syncState()


        val bottom_wear: MutableList<String> = ArrayList()
        bottom_wear.add("Jeans")
        bottom_wear.add("Tracks & Trousers")

        val foot_wear: MutableList<String> = ArrayList()
        foot_wear.add("Casuals")
        foot_wear.add("Formals")

        val upper_wear: MutableList<String> = ArrayList()
        upper_wear.add("T-Shirts")
        upper_wear.add("Shirts")


        val mobile: MutableList<String> = ArrayList()
        mobile.add("Apple")
        mobile.add("Samsung")

        val laptops: MutableList<String> = ArrayList()
        laptops.add("Dell")
        laptops.add("Toshiba")

        val ranking: MutableList<String> = ArrayList()
        ranking.add("Most Viewed Products")
        ranking.add("Most Ordered Products")
        ranking.add("Most Shared Products")

        menu.add("Bottom Wear")
        menu.add("Upper Wear")
        menu.add("Foot Wear")
        menu.add("Mobiles")
        menu.add("Laptops")
        menu.add("Ranking")

        subMenu.add(bottom_wear)
        subMenu.add(upper_wear)
        subMenu.add(foot_wear)
        subMenu.add(mobile)
        subMenu.add(laptops)
        subMenu.add(ranking)



        navList.setAdapter(ExpandableListAdapter(this, navList, menu, subMenu,this))


    }

    private fun showAllItemAPI() {

        var mItemsListURL = Endpoints().getItemsListURL()
        Log.d(TAG, "mRandomUserURL : $mItemsListURL")

        VolleySingleton.getInstance(this).addToRequestQueue(
            JsonObjectRequest(
                Request.Method.GET, mItemsListURL, null,
                Response.Listener { response ->
                    try {

                        Log.d(TAG, "json response for items list  $response")
                        onItemListSuccess(response)

                    } catch (e: JSONException) {
                        Log.d(TAG, "Exception in items list API ${e.message}")
                    }

                },
                Response.ErrorListener {
                    Log.d(TAG, "Error in json response for items list  ${it.message}")
                })
        )

    }

    private fun onItemListSuccess(response: JSONObject?) {

        var mItemData: ItemsListResponse? =
            gson.fromJson(response.toString(), ItemsListResponse::class.java)

        categoryItemsListArrayList = mItemData!!.categories

        for (i in 0 until categoryItemsListArrayList.size) {

            productItemsListArrayList = categoryItemsListArrayList[0].products

            for (j in 0 until categoryItemsListArrayList[i].products.size) {

                Log.d(TAG, " size : ${categoryItemsListArrayList[i].products.size}")
                productItemsListArrayList.add(categoryItemsListArrayList[i].products[j])
            }
        }

        d(TAG, " fetched data : " + productItemsListArrayList.size)

        onUserListShowSuccess(productItemsListArrayList)
        id_progress_bar_items_list.visibility = View.GONE
    }

    private fun showFilteredItems(selectedValue: String?) {

        var mItemsListURL = Endpoints().getItemsListURL()
        Log.d(TAG, "mRandomUserURL : $mItemsListURL")

        VolleySingleton.getInstance(this).addToRequestQueue(
            JsonObjectRequest(
                Request.Method.GET, mItemsListURL, null,
                Response.Listener { response ->
                    try {

                        Log.d(TAG, "json response for items list  $response")
                        onFilteredItemListSuccess(response,selectedValue)

                    } catch (e: JSONException) {
                        Log.d(TAG, "Exception in items list API ${e.message}")
                    }

                },
                Response.ErrorListener {
                    Log.d(TAG, "Error in json response for items list  ${it.message}")
                })
        )
    }

    private fun onFilteredItemListSuccess(response: JSONObject?, selectedValue: String?) {

        var mItemData: ItemsListResponse? =
            gson.fromJson(response.toString(), ItemsListResponse::class.java)

        categoryItemsListArrayList.clear()
        productItemsListArrayList.clear()

        categoryItemsListArrayList = mItemData!!.categories

        for (i in 0 until categoryItemsListArrayList.size) {

            var filteredNames = mItemData!!.categories[i].name
            if (filteredNames.equals(selectedValue)){

                for (j in 0 until categoryItemsListArrayList[i].products.size) {

                    productItemsListArrayList.add(categoryItemsListArrayList[i].products[j])
                }

            }

            onUserListShowSuccess(productItemsListArrayList)
            id_progress_bar_items_list.visibility = View.GONE
        }

    }

    private fun onUserListShowSuccess(productItemsListArrayList: ArrayList<Product>) {

        recyclerViewItemsList.layoutManager = GridLayoutManager(this, 2)
        mItemsListAdapter = ItemsListAdapter(productItemsListArrayList, this)
        recyclerViewItemsList.adapter = mItemsListAdapter
    }

    override fun onBackPressed() {

        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        /* when (item.itemId) {
             R.id.nav_home -> {
                 // Handle the camera action
             }
             R.id.nav_gallery -> {

             }
             R.id.nav_slideshow -> {

             }
             R.id.nav_tools -> {

             }
             R.id.nav_share -> {

             }
             R.id.nav_send -> {

             }
         }*/

        drawerLayout!!.closeDrawer(GravityCompat.START)
        return true
    }
}




