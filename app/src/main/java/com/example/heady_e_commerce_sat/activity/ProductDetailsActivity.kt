package com.example.heady_e_commerce_sat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.heady_e_commerce_sat.R
import kotlinx.android.synthetic.main.activity_product_details.*

class ProductDetailsActivity : AppCompatActivity() {

    val TAG = ProductDetailsActivity::class.java!!.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)

        init()
    }

    private fun init() {

        val actionbar = supportActionBar
        actionbar!!.title = "Detailed Items"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        if(intent.hasExtra("productName") ||
            intent.hasExtra("productPrice") ||
            intent.hasExtra("productSize") ||
            intent.hasExtra("productColor") ||
            intent.hasExtra("taxName") ||
            intent.hasExtra("taxValue")){

            var item_name = intent.getStringExtra("productName")
            tvItemName.text = item_name

            var item_price = intent.getStringExtra("productPrice")
            tvItemCost.text = item_price

            var item_size = intent.getIntExtra("productSize",0)
            tvItemSize.text = item_size.toString()

            var item_color = intent.getStringExtra("productColor")
            tvItemColor.text = item_color

            var taxName = intent.getStringExtra("taxName")
            tvTaxName.text = taxName + " : "

            var taxValue = intent.getFloatExtra("taxValue",0.0f)
            tvTaxValue.text = taxValue.toString() + " % "

            Log.d(TAG, "productName : $item_name  productPrice : $item_price productSize : $item_size" +
                    "productColor : $item_color  taxName : $taxName  taxValue : $taxValue")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
