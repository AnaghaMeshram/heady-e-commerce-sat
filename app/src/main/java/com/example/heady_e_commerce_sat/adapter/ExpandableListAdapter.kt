package com.example.heady_e_commerce_sat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView
import android.widget.Toast
import com.example.heady_e_commerce_sat.R

/**
 * Created by Anagha Meshram on 2019-06-29
 */
class ExpandableListAdapter(var context : Context,
                            var expandableListView : ExpandableListView,
                            var menu : MutableList<String>,
                            var subMenu : MutableList<MutableList<String>>,
                            var listener: SubCategoryClickListener?) : BaseExpandableListAdapter() {

    internal var mSubCategoryClickListener: SubCategoryClickListener? = null

    init {

        if (listener != null)
            this.mSubCategoryClickListener = listener
    }

    override fun getGroup(groupPosition: Int): String {
        return menu [groupPosition]
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        if(convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.list_group,null)
        }
        val title = convertView?.findViewById<TextView>(R.id.listTitle)
        title?.text = getGroup(groupPosition)
        title?.setOnClickListener {
            if(expandableListView.isGroupExpanded(groupPosition))
                expandableListView.collapseGroup(groupPosition)
            else
                expandableListView.expandGroup(groupPosition)
            //Toast.makeText(context, getGroup(groupPosition), Toast.LENGTH_SHORT).show()
        }
        return convertView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return subMenu[groupPosition].size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return subMenu[groupPosition][childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {

        var convertView = convertView
        if(convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.list_item,null)
        }
        val title = convertView?.findViewById<TextView>(R.id.expandableListItem)
        title?.text = getChild(groupPosition,childPosition).toString()
        title?.setOnClickListener {

            var selectedValue = getChild(groupPosition,childPosition).toString()
            mSubCategoryClickListener!!.onSubmenuClick(selectedValue)
            //Toast.makeText(context, getChild(groupPosition,childPosition).toString(),Toast.LENGTH_SHORT).show()
        }
        return convertView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return menu.size
    }

    interface SubCategoryClickListener {
        fun onSubmenuClick(selectedValue : String?)

    }
}