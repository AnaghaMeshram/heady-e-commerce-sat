package com.example.heady_e_commerce_sat.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.heady_e_commerce_sat.R
import com.example.heady_e_commerce_sat.activity.ProductDetailsActivity
import com.example.heady_e_commerce_sat.model.Category
import com.example.heady_e_commerce_sat.model.Product
import com.example.heady_e_commerce_sat.model.Variants
import java.lang.Exception

/**
 * Created by Anagha Meshram on 2019-06-26
 */

class ItemsListAdapter(val itemList: ArrayList<Product>, val context: Context) : RecyclerView.Adapter<ItemsListAdapter.ViewHolder>() {

    val TAG = ItemsListAdapter::class.java!!.simpleName
    var name : String? = null
    var price : Int? = null
    var size : Int? = null
    var color : String? = null
    var taxName : String? = null
    var taxValue : Float? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsListAdapter.ViewHolder {
        val view = LayoutInflater.from(parent?.context)
            .inflate(R.layout.cardview_layout_items_list,parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemsListAdapter.ViewHolder, position: Int) {

        val itemListData : Product = itemList[position]

        try {

            for (i in 0 until itemList.size) {

                name = itemListData.name
                holder?.tvItemName.text = name

                taxName = itemListData.tax.name
                taxValue = itemListData.tax.value

                var variantsArrayList = itemListData.variants

                for (j in 0 until variantsArrayList.size) {

                    price = itemListData.variants[0].price
                    holder?.tvItemCost.text = "Rs. ${price.toString()}"

                    size = itemListData.variants[0].size
                    color = itemListData.variants[0].color

                }
            }

        }catch (e : Exception){
            Log.e(TAG,"Error in Items List Adapter ${e.message}")
        }

        holder?.cardItems.setOnClickListener{

            val intent = Intent(context, ProductDetailsActivity::class.java)
            var productName = holder?.tvItemName.text.toString()
            var productPrice = holder?.tvItemCost.text.toString()
            intent.putExtra("productName",productName)
            intent.putExtra("productPrice",productPrice)
            intent.putExtra("productSize",size)
            intent.putExtra("productColor",color)
            intent.putExtra("taxName",taxName)
            intent.putExtra("taxValue",taxValue)
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return  itemList.size
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        val tvItemName  = itemView.findViewById<TextView>(R.id.tvItemName)
        val tvItemCost  = itemView.findViewById<TextView>(R.id.tvItemCost)
        val cardItems = itemView.findViewById<CardView>(R.id.cardItems)

    }
}